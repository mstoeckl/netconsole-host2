// copy of netconsole-host's functionality with some improvements

#include "stdio.h"
#include <vector>
#include <net/if.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>
#include <ifaddrs.h>
#include <time.h>
#include <stdlib.h>
#include <memory.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <signal.h>

bool operator<(const struct timespec& a, const struct timespec& b) {
	return a.tv_sec < b.tv_sec
			|| ((a.tv_sec == b.tv_sec) && (a.tv_nsec < b.tv_nsec));
}
struct timespec operator-(const struct timespec& a, int32_t b) {
	struct timespec s;
	if (a.tv_nsec > b) {
		s.tv_sec = a.tv_sec;
		s.tv_nsec = a.tv_nsec - b;
	} else {
		s.tv_sec = a.tv_sec - 1;
		s.tv_nsec = 1000*1000*1000+a.tv_nsec - b;
	}
	return s;
}

std::vector<in_addr> getBroadcastAddrs() {
	static struct timespec lt = { 0, 0 };
	static std::vector<in_addr> items;

	struct timespec tp;
	clock_gettime(CLOCK_REALTIME, &tp);
	const int32_t PERIOD = 100 * 1000 * 1000;
	if ( (tp - PERIOD) < lt) {

	}
	if ((tp.tv_sec == lt.tv_sec && tp.tv_nsec - lt.tv_nsec < PERIOD)
			|| (tp.tv_sec == lt.tv_sec + 1
					&& tp.tv_nsec + 1000 * 1000 * 1000 - lt.tv_nsec < PERIOD)) {
		return items;
	}

	items.clear();
	struct ifaddrs* addrs;
	getifaddrs(&addrs);
	for (struct ifaddrs* ifa = addrs; ifa != NULL; ifa = ifa->ifa_next) {
		if (ifa->ifa_addr == 0) {
			continue;
		}
		if (ifa->ifa_addr->sa_family != AF_INET) {
			continue;
		}
		if (ifa->ifa_flags & IFF_BROADCAST) {
			sockaddr_in* addr = (sockaddr_in*) ifa->ifa_ifu.ifu_broadaddr;
			items.push_back(addr->sin_addr);
		}
	}
	freeifaddrs(addrs);
	return items;
}

const char* getAvailableLogfile() {
	size_t opts = 3;
	const char* options[opts];
#if !0
	options[0] = "log1.log";
	options[1] = "log2.log";
	options[2] = "log3.log";
#else
	options[0] = "/var/log/robotlog1.log";
	options[1] = "/var/log/robotlog2.log";
	options[2] = "/var/log/robotlog3.log";
#endif

	struct timespec earliest = { 0, 0 };
	const char* choice = nullptr;
	for (size_t i = 0; i < opts; i++) {
		struct stat info;
		if (stat(options[i], &info) < 0) {
			// file does not yet exist
			return options[i];
		} else {
			// file with earliest modification time
			struct timespec comp = info.st_mtim;
			if (earliest.tv_nsec == 0 && earliest.tv_sec == 0) {
				earliest = comp;
				choice = options[i];
			} else if (earliest.tv_sec > comp.tv_sec
					|| (earliest.tv_sec == comp.tv_sec
							&& earliest.tv_nsec > comp.tv_nsec)) {
				earliest = comp;
				choice = options[i];
			}
		}
	}

	return choice;
}

int main(int argc, char** argv) {
	if (argc != 2) {
		printf("Usage: netconsole-host2 [file to run]\n");
		return 0;
	}

	const char* program = argv[1];
	const char* logfile = getAvailableLogfile();

	int des_p[2];
	pipe(des_p);

	int r = fork();
	if (r == 0) {
		close(STDOUT_FILENO);          //closing stdout
		close(STDERR_FILENO);
		dup2(des_p[1], STDOUT_FILENO);
		dup2(des_p[1], STDERR_FILENO);
		close(des_p[0]);   //closing pipe read

		int ret = execv(program, &argv[1]);
		if (ret == -1) {
			printf("Error: execv failed\n");
		}
		return 0;
	} else if (r == -1) {
		printf("Error: fork failed\n");
		return -1;
	}
	close(des_p[1]);

	size_t buffersize = 1 << 16;
	char buffer[buffersize];
	ssize_t amount = 0;

	FILE* log = fopen(logfile, "w");
	setvbuf(log, nullptr, _IONBF, 0);
	sockaddr_in sa;
	sa.sin_port = htons(6666);
	sa.sin_family = AF_INET;
	memset(&sa.sin_zero, 0, 8);

	int sock = socket(AF_INET, SOCK_DGRAM, 0);
	int optval = 1;
	setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &optval, sizeof(optval));
	// parent seems to fall off radar -- does it not read well?
	while (kill(r, 0) == 0) {
		amount = read(des_p[0], buffer, buffersize);
		write(STDOUT_FILENO, (const void*) buffer, amount);
		fwrite((const void*) buffer, 1, amount, log);

		for (in_addr saddr : getBroadcastAddrs()) {
			sa.sin_addr = saddr;
			sendto(sock, buffer, amount, 0, (struct sockaddr*) &sa, sizeof(sa));
		}
	}
	printf("Reading from child failed\n");
	close(sock);
	fclose(log);
	close(des_p[0]);
}
